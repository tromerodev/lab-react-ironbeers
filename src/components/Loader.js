import React from "react";

export const Loader = () => {
  return (
    <div className="pageloader is-active is-warning">
      <span className="title">Loading...</span>
    </div>
  );
};
